# Scaling in Kubernetes

Kubernetes auto-scaling refers to the ability of the platform to automatically adjust the number of running instances, known as pods, based on the observed resource utilization or application demand. It allows your applications to scale horizontally by adding or removing pods dynamically, ensuring that your services are responsive and can handle increased traffic or workload.

## Types of Auto-Scaling in Kubernetes

- **Horizontal Pod** Auto-Scaling: HPA automatically adjusts the number of pod replicas based on CPU utilization, memory usage, or custom metrics. It ensures that your applications have sufficient resources to handle the workload efficiently. HPA allows you to set minimum and maximum replica limits, as well as define target utilization thresholds, enabling fine-grained control over your application's scaling behaviour. 

- **Vertical Pod Auto-Scaling**: VPA focuses on optimizing resource allocation within individual pods. It analyzes historical resource utilization patterns and adjusts resource requests and limits accordingly. By dynamically adjusting CPU and memory allocations, VPA helps improve resource utilization and reduce wastage, ultimately leading to cost savings and improved performance.  

## How to do request based auto-scaling in Kubernetes 

